<!DOCTYPE HTML>
<!--
	Multiverse by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Valeria Astor Nebot</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<h1><a href="index.html">Valeria Astor Nebot</a></h1>
						<nav>
							<ul>
								<li><a href="#footer" class="icon fa-info-circle">Quién soy</a></li>
							</ul>
						</nav>
					</header>

				<!-- Main -->
					<div id="main">
						<article class="thumb">
							<a href="images/fulls/tickantel.png" class="image"><img src="images/thumbs/tickantel.png" alt="" /></a>
							<h2>tickantel</h2>
							<p>Diseño - Maquetado (Bootstrap)</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/02.jpg" class="image"><img src="images/thumbs/02.jpg" alt="" /></a>
							<h2>vera tv</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/03.jpg" class="image"><img src="images/thumbs/03.jpg" alt="" /></a>
							<h2>parlamento</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/04.jpg" class="image"><img src="images/thumbs/04.jpg" alt="" /></a>
							<h2>ingesur</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/05.jpg" class="image"><img src="images/thumbs/05.jpg" alt="" /></a>
							<h2>dulces cristel</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/06.jpg" class="image"><img src="images/thumbs/06.jpg" alt="" /></a>
							<h2>laboratorio analítico del este</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/07.jpg" class="image"><img src="images/thumbs/07.jpg" alt="" /></a>
							<h2>Mauris id tellus arcu</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/08.jpg" class="image"><img src="images/thumbs/08.jpg" alt="" /></a>
							<h2>Nunc vehicula id nulla</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/09.jpg" class="image"><img src="images/thumbs/09.jpg" alt="" /></a>
							<h2>Neque et faucibus viverra</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/10.jpg" class="image"><img src="images/thumbs/10.jpg" alt="" /></a>
							<h2>Mattis ante fermentum</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/11.jpg" class="image"><img src="images/thumbs/11.jpg" alt="" /></a>
							<h2>Sed ac elementum arcu</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
						<article class="thumb">
							<a href="images/fulls/12.jpg" class="image"><img src="images/thumbs/12.jpg" alt="" /></a>
							<h2>Vehicula id nulla dignissim</h2>
							<p>Nunc blandit nisi ligula magna sodales lectus elementum non. Integer id venenatis velit.</p>
						</article>
					</div>

				<!-- Footer -->
					<footer id="footer" class="panel">
						<div class="inner split">
							<div>
								<section>
									<h2>Hola!</h2>
									<p>Mi nombre es Valeria Astor Nebot.
										<br/>Tengo 36 años y soy oriunda de las sierras de Minas.
										<br/>Soy diseñadora gráfica especializada en desarrollos web.</p>
								</section>
								<section>
									<ul class="icons">
										<li><a href="http://uy.linkedin.com/in/valeriaastor" class="icon fa-linkedin"><span class="label">LinkedIn</span></a></li>
									</ul>
								</section>
							</div>
							<div>
								<section>
									<h2>Contacto</h2>

									<?php ?>

										<form action="index.php" method="post" onsubmit="return validar(this)">
											<?php include ('metodos.php'); ?>

											<div class="msg-error hide msg">Se produjo un error.</div>
											<div class="msg-ok hide msg">Tu mensaje se ha enviado correctamente. Gracias</div>

											<fieldset>
												<legend class="hide">Envío de consulta</legend>

												<div class="combo-form nombre">
													<label for="nombre">Nombre *</label>
													<input type="text" id="nombre" name="nombre" value="<?php echo $nombre; ?>">
													<span class="msgError hide">Error</span>
												</div>

												<div class="combo-form email">
													<label for="email">Email *</label>
													<input type="text" id="email" name="email" value="<?php echo $email; ?>">
													<span class="msgError hide">Error</span>
												</div>

												<div class="combo-form tel">
													<label for="tel">Teléfono *</label>
													<input type="text" id="tel" name="tel" value="<?php echo $tel; ?>">
													<span class="msgError hide">Error</span>
												</div>

												<div class="combo-form">
													<label for="asunto">Asunto *</label>
													<select name="asunto">
														<option value="reserva">Reserva</option>
														<option value="pedido">Pedido</option>
														<option value="sugerencia">Sugerencias</option>
													</select>
													<span class="msgError hide">Error</span>
												</div>

												<div class="combo-form comentarios">
													<label for="comentarios">Comentarios</label>
													<textarea id="comentarios" name="comentarios" value="<?php echo $comentarios; ?>"></textarea>
													<span class="msgError hide">Error</span>
												</div>

												<div class="combo-form">
													<input type="submit" id="enviar" name="enviar">
												</div>
											</fieldset>
										</form>




									<form method="post" action="#">
										<div class="field half first">
											<input type="text" name="name" id="name" placeholder="nombre" />
										</div>
										<div class="field half">
											<input type="text" name="email" id="email" placeholder="correo electrónico" />
										</div>
										<div class="field">
											<textarea name="message" id="message" rows="4" placeholder="mensaje"></textarea>
										</div>
										<ul class="actions">
											<li><input type="submit" value="enviar" class="special" /></li>
										</ul>
									</form>
								</section>
							</div>
						</div>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

			<script src="js/validaciones.js"></script>

	</body>
</html>
