<?php
	
	$nombre = "";
	$email = "";
	$tel = "";
	$comentarios = "";
	
	$errores = "";
	
	if(isset($_POST["enviar"])){
		
		$errores = true;

		$nombre = strip_tags($_POST["nombre"]);
		$email = strip_tags($_POST["email"]);
		$tel = strip_tags($_POST["tel"]);
		$asunto = strip_tags($_POST["asunto"]);
		$comentarios = strip_tags($_POST["comentarios"]);
		
		$errores = 	validarString("nombre", $nombre) |
					validarCorreo("email", $email)   |
					validarTextarea("comentarios", $comentarios);					
					
		if($errores){
			
			$email_to = "info@quintobar.com";
			$email_from = $email;
			$email_subject = "Contacto desde el sitio web";
					
			$email_message = "Detalles del formulario de contacto:\n\n";
			$email_message .= "nombre: " . $_POST['nombre'] . "\n";
			$email_message .= "email: " . $_POST['email'] . "\n";
			$email_message .= "tel: " . $_POST['tel'] . "\n";
			$email_message .= "Asunto: " . $_POST['asunto'] . "\n";
			$email_message .= "comentarios: " . $_POST['comentarios'] . "\n";

			$headers =  'From: '.$email_from."\r\n".'Reply-To: '.$email_from."\r\n" .'X-Mailer: PHP/' . phpversion();
			@mail($email_to, $email_subject, $email_message, $headers);		
			
			echo
				"<script>
					$(document).ready(function(){
						$('div.msg-ok').show();
						$('html, body').stop().animate({
							scrollTop: $('div.msg-ok').offset().top - 100
							}, 1500, 'easeInOutExpo');
						});
				</script>";
				
			$nombre = "";
			$email = "";
			$tel = "";
			$comentarios = "";	
			
		}
	}	
	
	function validarString ($nombreCampo, $valor) {
		
		Global $msgError;
		$largoPalabra = 150;
		
		if(($valor == '')){
			$msgError.= "El campo " . $nombreCampo . " esta vac&iacute;o"."</br>";
			marcarError($nombreCampo);
			return false;			
		}
			
		if(is_numeric($valor)){
			$msgError.= "El campo " . $nombreCampo . " tiene n&uacute;meros"."</br>";
			marcarError($nombreCampo);
			return false;
		}
		
		if (strlen($valor) > $largoPalabra){
			$msgError.= "El campo " . $nombreCampo . " esta muy largo"."</br>";
			marcarError($nombreCampo);
			return false;
		}
		
		return true;
	}
	
	function validarTextarea ($nombreCampo, $valor) {
		
		Global $msgError;
		$largoPalabra = 300;
		
		if(($valor == '')){
			$msgError.= "El campo " . $nombreCampo . " esta vac&iacute;o"."</br>";
			marcarError($nombreCampo);
			return false;			
		}
			
		if (strlen($valor) > $largoPalabra){
			$msgError.= "El campo " . $nombreCampo . " esta muy largo"."</br>";
			marcarError($nombreCampo);
			return false;
		}
		
		return true;
	}
	
	function validarCorreo($nombreCampo, $valor){
		
		Global $msgError;
		
		if(($valor == '')){
			$msgError.= "El campo " . $nombreCampo . " esta vac&iacute;o"."</br>";
			marcarError($nombreCampo);
			return false;			
		}
		
		if(!filter_var($valor, FILTER_VALIDATE_EMAIL)){
			$msgError.= "El " . $nombreCampo . " es inv&aacute;lido"."</br>";
			marcarError($nombreCampo);
			return false;
		}else{
			return true;
		}	
		
	}

?>