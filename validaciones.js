// 	Validaciones js
	
	function validar(f){
	
		var nombre = f.elements["nombre"].value;
		var email = f.elements["email"].value;
		var tel = f.elements["tel"].value;
		var asunto = f.elements["asunto"].value;		
		var comentarios = f.elements["comentarios"].value;	
		
		var valido = false;
		
		var ok = validarString("nombre",nombre);
		//ok = validarString("asunto",asunto) && ok;
		ok = validarString("tel",tel) && ok;
		ok = validarString("comentarios",comentarios) && ok;
		ok = validarCorreo("email",email) && ok;
		
		return ok;
		
		function validarString(clave, valor){
			var div_error = "div." + clave + " span.msgError";
			if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
				$("div." + clave).addClass('error');
				$(div_error).html('El campo está vacío');				
				return false;
			}else{
				$("div." + clave).removeClass('error');
				$(div_error).hide();
				return true;
			}
		}
		
		function validarCorreo(clave, valor){
			var div_error = "div." + clave + " span.msgError";
			var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if(  valor == null || valor.length == 0 || (!expr.test(valor)) ) {
				$("div." + clave).addClass('error');
				$(div_error).html('El correo no es válido');
				return false;
			}else{
				$("div." + clave).removeClass('error');
				$(div_error).hide();
				return true;
			}
		}	
		
	}